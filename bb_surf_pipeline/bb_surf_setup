
#!/bin/bash

##########
# Inputs #
##########
export subj="$1"
export study="$2"


################
# Tool folders #
################

# General tool folders

export MATLAB_COMPILER_RUNTIME="" # Complete with Matlab Runtime v96 Folder
export CARET7DIR="" # Complete with Workbench 1.4.2 bin folder
export FREESURFER_HOME="" # Complete with FreeSurfer folder
export MSMBINDIR="" # Complete with MSM bin folder
export HCPPIPEDIR="" # Complete with HCP pipelines folder
export FSL_FIX_MCR="" # Complete with Matlab Runtime v910 Folder
export FSL_FIX_MCRROOT="" # Complete with Matlab Runtime v910 Folder
export FSL_FIXDIR="${BB_BIN_DIR}/bb_functional_pipeline/bb_alt_fix_dir"

# HCP tool folders
export HCPPIPEDIR_Templates="${HCPPIPEDIR}/global/templates"
export HCPPIPEDIR_Config="${HCPPIPEDIR}/global/config"
export HCCPPIPEDIR_MNI="${HCPPIPEDIR}/global/MNINonLinear/"
export MSMCONFIGDIR="${HCPPIPEDIR}/MSMConfig"
export MSMAllTemplates="${HCPPIPEDIR}/global/templates/MSMAll"
export SubcorticalGrayLabels="${HCPPIPEDIR_Config}/FreeSurferSubcorticalLabelTableLut.txt"
export FreeSurferLabels="${HCPPIPEDIR_Config}/FreeSurferAllLut.txt"
export SurfaceAtlasDIR="${HCPPIPEDIR_Templates}/standard_mesh_atlases"
export GrayordinatesSpaceDIR="${HCPPIPEDIR_Templates}/91282_Greyordinates"
export ReferenceMyelinMaps="${SurfaceAtlasDIR}/Conte69.MyelinMap_BC.164k_fs_LR.dscalar.nii"



#############
# Constants #
#############

export GrayordinatesResolution="2"  #Usually 2mm
export HighResMesh="164"            #Usually 164k vertices
export LowResMesh="32"              #Usually 32k vertices
export FinalfMRIResolution="2"      #Needs to match what is in fMRIVolume, i.e. 2mm for 3T HCP data and 1.6mm for 7T HCP data
export SmoothingFWHM="2"
export MyelinMappingFWHM="5"
export SurfaceSmoothingFWHM="4"
export CorrectionSigma="$(echo "sqrt ( 200 )" | bc -l)"
export Sigma=$(echo "$SmoothingFWHM / ( 2 * ( sqrt ( 2 * l ( 2 ) ) ) )" | bc -l)
export MyelinMappingSigma=`echo "$MyelinMappingFWHM / ( 2 * ( sqrt ( 2 * l ( 2 ) ) ) )" | bc -l`
export SurfaceSmoothingSigma=`echo "$SurfaceSmoothingFWHM / ( 2 * ( sqrt ( 2 * l ( 2 ) ) ) )" | bc -l`
export MatlabMode="0" #Mode=0 compiled Matlab, Mode=1 interpreted Matlab, Mode=2 Octave
export LeftGreyRibbonValue="3"
export LeftWhiteMaskValue="2"
export RightGreyRibbonValue="42"
export RightWhiteMaskValue="41"
export HighPass="2000"


#########
# NAMES #
#########

# Not sure how to classify
export name_to_process="filtered_func_data_clean"
export fMRI_names="filtered_func_data_clean_MNI" # (UKB)
export mrfixNames=""
export mrfixConcatName=""
export mrfixNamesToUse=""
export fMRI_proc_STRING="_Atlas"
export in_reg_name="MSMSulc"
export out_reg_name="MSMAll_Strain"

# Image names
export T1_img_name="T1"
export T2_img_name="T2_FLAIR"
export T1_bm_img_name="T1_brain_mask"
export MNI_T1_img_name="T1"
export fMRI_img_name="filtered_func_data_clean"
export tfMRI_img_name="filtered_func_data"
export MNI_warp_img_name="T1_to_MNI_warp"
export inv_MNI_warp_img_name="MNI_to_T1_warp"
export SBRef_img_name="example_func2standard"
export output_fMRI_img_name="filtered_func_data_clean_MNI"
export output_tfMRI_img_name="filtered_task_func_data_MNI"
export input_spin_echo_mean_img_name="fieldmap_iout_mean_ud.nii.gz"
export input_spin_echo_mean_to_struct_mat_name="fieldmap_iout_to_T1.mat"
export input_spin_echo_mean_to_standard_img_name="fieldmap_iout_mean_ud_to_standard.nii.gz"
export input_FS_annot_filename="rh.aparc.a2009s.annot"
export task_fMRI_img_name="filtered_func_data"
export native_midthickness_surf_R_filename="bb.R.midthickness.native.surf.gii"
export native_midthickness_surf_L_filename="bb.L.midthickness.native.surf.gii"
export native_task_timeseries_R_filename="bb.R.task.native.func.gii"
export native_task_timeseries_L_filename="bb.L.task.native.func.gii"
export native_white_surf_R_filename="bb.R.white.native.surf.gii"
export native_white_surf_L_filename="bb.L.white.native.surf.gii"
export native_pial_surf_R_filename="bb.R.pial.native.surf.gii"
export native_pial_surf_L_filename="bb.L.pial.native.surf.gii"
export native_MSMAll_surf_warp_R_filename="bb.R.sphere.MSMAll_DeDrift.native.surf.gii"
export native_MSMAll_surf_warp_L_filename="bb.L.sphere.MSMAll_DeDrift.native.surf.gii"
export timeseries_MSMAll_surf_R_filename="bb.R.task.MSMAll_DeDrift.32k_fs_LR.func.gii"
export timeseries_MSMAll_surf_L_filename="bb.L.task.MSMAll_DeDrift.32k_fs_LR.func.gii"
export standard_32k_sphere_R_filename="bb.R.sphere.32k_fs_LR.surf.gii"
export standard_32k_sphere_L_filename="bb.L.sphere.32k_fs_LR.surf.gii"
export standard_32k_MSMAll_midthickness_surf_R_filename="bb.R.midthickness_MSMAll_DeDrift.32k_fs_LR.surf.gii"
export standard_32k_MSMAll_midthickness_surf_L_filename="bb.L.midthickness_MSMAll_DeDrift.32k_fs_LR.surf.gii"
export standard_subcortical_volume_labels_filename="Atlas_ROIs.2.nii.gz"
export standard_medial_wall_mask_R_filename="bb.R.atlasroi.32k_fs_LR.shape.gii"
export standard_medial_wall_mask_L_filename="bb.L.atlasroi.32k_fs_LR.shape.gii"
export task_cifti_filename="bb.task.MSMAll_DeDrift.32k_fs_LR.dtseries.nii"
export rest_cifti_filename="filtered_func_data_clean_MNI/filtered_func_data_clean_MNI_Atlas_MSMAll_DeDrift.dtseries.nii"
export tfMRI_sm2="bb.tfMRI.MSMAll_smooth_2.dtseries.nii"
export tfMRI_sm6="bb.tfMRI.MSMAll_smooth_6.dtseries.nii"


# Folder names
export output_dir_name="surf_fMRI"
export T1_dir_name="T1"
export T2_dir_name="T2_FLAIR"
export FS_dir_name="FreeSurfer"
export mri_dir_name="mri"
export fMRI_dir_name="fMRI"
export rfMRI_dir_name="rfMRI.ica"
export task_fMRI_dir_name="tfMRI.feat"
export rfMRI_regS_dir_name="reg_standard"
export tfMRI_regS_dir_name="reg_standard"
export rfMRI_reg_dir_name="reg"
export tfMRI_reg_dir_name="reg"
export native_dir_name="Native"
export FS_avg_dir_name="fsaverage"
export FS_avg32K_dir_name="${FS_avg_dir_name}_LR32k"
export T1_transforms_dir_name="transforms"
export RV_to_SM_dir_name="RibbonVolumeToSurfaceMapping"
export MNI_dir_name="MNINonLinear"
export MSM_sulc_dir_name="MSMSulc"
export L_logdir_name="L.logdir"
export R_logdir_name="R.logdir"
export MNI_ROI_dir_name="ROIs"
export results_dir_name="Results"
export results_clean_dir_name="filtered_func_data_clean"
export bias_dir_name="BFC"
export DownSample_dir_name="fsaverage_LR${LowResMesh}k"
export MNI_dense_TS_dir_name="${fMRI_names}_Atlas"
export input_fieldmap_dir_name="fieldmap"
export input_FS_labels_dir_name="${FS_dir_name}/label/"




# Dedrift variables
export dedrift_reg_name="MSMAll_Strain_2_d40_WRN"
export dedrift_reg_files="${HCCPPIPEDIR_MNI}/DeDriftMSMAllUKB.L.sphere.DeDriftMSMAllUKB.164k_fs_LR.surf.gii@${HCCPPIPEDIR_MNI}/DeDriftMSMAllUKB.R.sphere.DeDriftMSMAllUKB.164k_fs_LR.surf.gii"
export dedrift_concat_reg_name="MSMAll_DeDrift"
export dedrift_maps="sulc curvature corrThickness thickness"
export dedrift_myelin_maps="MyelinMap SmoothedMyelinMap" #No _BC, this will be reapplied
export dedrift_MRFixConcatNames="NONE"
export dedrift_MRFixNames="NONE"
export dedrift_fixNames="filtered_func_data_clean_MNI" #Space delimited list or NONE
export dedrift_dontFixNames="" 
export dedrift_HighPass="-1"
export dedrift_MotionRegression="FALSE"

export dedrift_maps=`echo "$dedrift_maps" | sed s/" "/"@"/g`
export dedrift_myelin_maps=`echo "$dedrift_myelin_maps" | sed s/" "/"@"/g`
export dedrift_MRFixNames=`echo "$dedrift_MRFixNames" | sed s/" "/"@"/g`
export dedrift_fixNames=`echo "$dedrift_fixNames" | sed s/" "/"@"/g`
export dedrift_dontFixNames=`echo "$dedrift_dontFixNames" | sed s/" "/"@"/g`






#################
# Subject paths #
#################

# General subject path
export subject_dir="${study}/${subj}"

# Input folder paths
export input_T1_dir="${subject_dir}/${T1_dir_name}"
export input_T2_dir="${subject_dir}/${T2_dir_name}"
export input_T1_transforms_dir="${input_T1_dir}/${T1_transforms_dir_name}"
export input_fMRI_dir="${subject_dir}/${fMRI_dir_name}"
export input_rfMRI_dir="${input_fMRI_dir}/${rfMRI_dir_name}"
export input_tfMRI_dir="${input_fMRI_dir}/${task_fMRI_dir_name}"
export input_rfMRI_regS_dir="${input_rfMRI_dir}/${rfMRI_regS_dir_name}"
export input_tfMRI_regS_dir="${input_tfMRI_dir}/${tfMRI_regS_dir_name}"
export input_rfMRI_reg_dir="${input_rfMRI_dir}/${rfMRI_reg_dir_name}"
export input_tfMRI_reg_dir="${input_tfMRI_dir}/${tfMRI_reg_dir_name}"
export input_FS_dir="${subject_dir}/${FS_dir_name}"
export input_FS_mri_dir="${input_FS_dir}/${mri_dir_name}"
export input_fieldmap_dir="${subject_dir}/${input_fieldmap_dir_name}"
export input_FS_labels_dir="${subject_dir}/${input_FS_labels_dir_name}"

# Output T1 folder Paths
export output_dir="${subject_dir}/${output_dir_name}"
export output_T1_dir="${output_dir}/${T1_dir_name}"
export output_T1_native_dir="${output_T1_dir}/${native_dir_name}"
export output_T1_FS_avg32K_dir="${output_T1_dir}/${FS_avg32K_dir_name}"
export output_T1_transforms_dir="${output_T1_dir}/${T1_transforms_dir_name}"
export output_T1_bias_dir="${output_T1_dir}/${bias_dir_name}"

#Output T2 folder paths
export output_T2_dir="${output_dir}/${T2_dir_name}"

# Output working directory path
export output_RV_to_SM_dir="${output_dir}/${RV_to_SM_dir_name}"

# Output atlas folder path
export output_MNI_dir="${output_dir}/${MNI_dir_name}"
export output_MNI_native_dir="${output_MNI_dir}/${native_dir_name}"
export output_MSM_sulc_dir="${output_MNI_native_dir}/${MSMSulc_dir_name}"
export output_MSM_sulc_L_log_dir="${output_MSM_sulc_dir}/${L_logdir_name}"
export output_MSM_sulc_R_log_dir="${output_MSM_sulc_dir}/${R_logdir_name}"
export output_MNI_ROI_dir="${output_MNI_dir}/${MNI_ROI_dir_name}"
export output_MNI_results_dir="${output_MNI_dir}/${results_dir_name}"
export output_MNI_results_clean_MNI_dir="${output_MNI_results_dir}/${output_fMRI_img_name}"
export output_MNI_results_clean_dir="${output_MNI_results_dir}/${results_clean_dir_name}"
export output_MNI_dense_TS_dir="${output_MNI_results_clean_MNI_dir}/${MNI_dense_TS_dir_name}"
export output_MNI_FS_avg_dir="${output_MNI_dir}/${FS_avg_dir_name}"
export output_MNI_FS_avg32K_dir="${output_MNI_dir}/${FS_avg32K_dir_name}"
export output_DownSample_dir="${output_MNI_FS_avg32K_dir}"


# Image paths
export input_T1_img="${input_T1_dir}/${T1_img_name}"
export input_T2_img="${input_T2_dir}/${T2_img_name}"
export T1_img="${output_T1_dir}/${T1_img_name}"
export T2_img="${output_T2_dir}/${T2_img_name}"
export MNI_warp_img="${input_T1_transforms_dir}/${MNI_warp_img_name}"
export fMRI_img="${input_rfMRI_regS_dir}/${fMRI_img_name}"
export tfMRI_img="${input_tfMRI_dir}/${tfMRI_img_name}"
export fMRI_MNI_img="${output_MNI_results_clean_MNI_dir}/${output_fMRI_img_name}"
export tfMRI_MNI_img="${output_MNI_results_clean_MNI_dir}/${output_tfMRI_img_name}"
export SBRef_img="${input_rfMRI_reg_dir}/${SBRef_img_name}"
export inv_MNI_warp_img="${output_T1_transforms_dir}/${inv_MNI_warp_img_name}"
export input_func2highres="${input_rfMRI_reg_dir}/${SBRef_img_name}.mat"
export input_func2highres_task="${input_tfMRI_reg_dir}/${SBRef_img_name}.mat"
export input_spin_echo_mean_img="${input_fieldmap_dir}/${input_spin_echo_mean_img_name}"
export input_spin_echo_mean_to_struct_mat="${input_fieldmap_dir}/${input_spin_echo_mean_to_struct_mat_name}"
export input_spin_echo_mean_to_standard_img="${input_fieldmap_dir}/${input_spin_echo_mean_to_standard_img_name}"
export input_FS_annot_file="${input_FS_labels_dir}/${input_FS_annot_filename}"
export input_task_fMRI_img="${input_tfMRI_dir}/${task_fMRI_img_name}"
export output_native_midthickness_surf_R="${output_MNI_native_dir}/${native_midthickness_surf_R_filename}"
export output_native_midthickness_surf_L="${output_MNI_native_dir}/${native_midthickness_surf_L_filename}"
export output_native_task_timeseries_R="${output_MNI_native_dir}/${native_task_timeseries_R_filename}"
export output_native_task_timeseries_L="${output_MNI_native_dir}/${native_task_timeseries_L_filename}"
export output_native_white_surf_R="${output_MNI_native_dir}/${native_white_surf_R_filename}"
export output_native_white_surf_L="${output_MNI_native_dir}/${native_white_surf_L_filename}"
export output_native_pial_surf_R="${output_MNI_native_dir}/${native_pial_surf_R_filename}"
export output_native_pial_surf_L="${output_MNI_native_dir}/${native_pial_surf_L_filename}"
export output_native_MSMAll_surf_warp_R="${output_MNI_native_dir}/${native_MSMAll_surf_warp_R_filename}"
export output_native_MSMAll_surf_warp_L="${output_MNI_native_dir}/${native_MSMAll_surf_warp_L_filename}"
export output_timeseries_MSMAll_surf_R="${output_MNI_native_dir}/${timeseries_MSMAll_surf_R_filename}"
export output_timeseries_MSMAll_surf_L="${output_MNI_native_dir}/${timeseries_MSMAll_surf_L_filename}"

export standard_32k_sphere_R="${output_MNI_FS_avg32K_dir}/${standard_32k_sphere_R_filename}"
export standard_32k_sphere_L="${output_MNI_FS_avg32K_dir}/${standard_32k_sphere_L_filename}"
export standard_32k_MSMAll_midthickness_surf_R="${output_MNI_FS_avg32K_dir}/${standard_32k_MSMAll_midthickness_surf_R_filename}"
export standard_32k_MSMAll_midthickness_surf_L="${output_MNI_FS_avg32K_dir}/${standard_32k_MSMAll_midthickness_surf_L_filename}"
export standard_subcortical_volume_labels="${output_MNI_ROI_dir}/${standard_subcortical_volume_labels_filename}"
export standard_medial_wall_mask_R="${output_MNI_FS_avg32K_dir}/${standard_medial_wall_mask_R_filename}"
export standard_medial_wall_mask_L="${output_MNI_FS_avg32K_dir}/${standard_medial_wall_mask_L_filename}"

export output_task_cifti="${output_MNI_results_dir}/${task_cifti_filename}"
export output_rest_cifti="${output_MNI_results_dir}/${rest_cifti_filename}"


# Final PATH export
export PATH="$CARET7DIR:$PATH"
