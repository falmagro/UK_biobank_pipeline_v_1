function bb_netmats(subj, wd, output_dir, output_basename, dims, dr_dirs, noise_components, tr, num_points, scalings, ridgep_param)
    % Needed env variables: BB_BIN_DIR and FSLDIR
    % Needed input parameters: 
    %   - subj: Subject with the data (also, name of the folder of said subject
    %   - wd: Working dir
    %   - output_dir: Folder to save the outputs
    %   - output_basenmae: Basename for the outputs
    %   - dims: Array with the dimensionalities to process
    %   - dr_dir: Folder with 1 txt file with the timecouse from dual regression step 1
    %   - noise_components: List of lists of components with noise
    %   - tr: 0.735
    %   - num_points: 490
    %   - scalings: Scaling parameters needed for the correlations
    %   - ridgep_param: Ridgep parameter
    %   
    %   Examples call from shell (This example has not specified noisy components:
    %   bb_surf_ICA_dr $subjID $PWD $outputDir rfMRI_test '{25, 100}' \
    %       '{'\''$subjID/surf_fMRI/ukbb_dim25.dr/timecourses/'\'','\''$subjID/surf_fMRI/ukbb_dim50.dr/timecourses/'\''}' \
    %       '{[],[]},' 0.735 490 '{[10.6484,10.6707],[19.7177,18.8310]}' 0.1         
    %

    if (isdeployed==false)      
        addpath(sprintf('%s/bb_functional_pipeline/bb_ICA_dr_dir/FSLNets',getenv('BB_BIN_DIR')))   
        addpath(sprintf('%s/etc/matlab',getenv('FSLDIR')))
    end

    subj_dir=strcat(wd,'/',subj);

    eval(strcat('dims = ', dims, ';'));
    eval(strcat('dr_dirs = ', dr_dirs, ';'));
    eval(strcat('noise_components = ', noise_components, ';'));
    eval(strcat('scalings = ', scalings, ';'));
    eval(strcat('ridgep_param = ', ridgep_param, ';'));

    num_dims = length(dims);
    
    if (length(dr_dirs) ~= num_dims) || (length(noise_components) ~= num_dims) || (length(scalings) ~= num_dims)
        'There was a problem in the inputs: Incorrect size or format'
    end
    
    for i = 1:num_dims
        D = dims{i};
        
        ts_dir=strcat(dr_dirs{i}) ;
        ts=nets_load(ts_dir,str2num(tr),0,1,str2num(num_points));
        
        ts.DD = [setdiff([1:D],noise_components{i})];
        r2zFULL=scalings{i}(1);
        r2zPARTIAL=scalings{i}(2);
        ts=nets_tsclean(ts,1);

        netmats1=  nets_netmats(ts, -r2zFULL,    'corr');
        netmats2=  nets_netmats(ts, -r2zPARTIAL, 'ridgep', ridgep_param);
       
        clear NET;
        
        output=strcat(output_dir, '/', output_basename, '_d', num2str(D));

        grot=reshape(netmats1(1,:),ts.Nnodes,ts.Nnodes); 
        NET(1,:)=grot(triu(ones(ts.Nnodes),1)==1); 
        
        po=fopen(strcat(output, '_fullcorr_v2.txt'),'w');
        fprintf(po,[ num2str(NET(1,:),'%14.8f') '\n']);  
        fclose(po);

        clear NET; 

        grot=reshape(netmats2(1,:),ts.Nnodes,ts.Nnodes); 
        NET(1,:)=grot(triu(ones(ts.Nnodes),1)==1); 

        po=fopen(strcat(output, '_partialcorr_v2.txt'),'w');
        fprintf(po,[num2str(NET(1,:),'%14.8f') '\n']);  
        fclose(po);

        ts_std=std(ts.ts);

        po=fopen(strcat(output, '_NodeAmplitudes_v2.txt'),'w');
        fprintf(po,[num2str(ts_std(1,:),'%14.8f') '\n']);  
        fclose(po);

    end


